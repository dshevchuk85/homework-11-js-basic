const registrationPasswordForm = document.querySelector('.password-form');
const submitButton = document.querySelector('.btn');

const password = document.querySelector(`[data-password='password']`);
const repeatPassword = document.querySelector(`[data-password='repeat-password']`);

const checkPasswordEquality = function (e) {
    e.preventDefault();
    if(password.value !== repeatPassword.value && password.value !== '') {
        document.querySelector('.error').innerText = 'Потрібно ввести однакові значення';
    } else {
        alert('You are welcome');
    }
}

registrationPasswordForm.addEventListener('click', (e) => {
    if (e.target !== e.currentTarget && e.target.classList.contains('icon-password')) {
        if (e.target.classList.contains('fa-eye')) {
            e.target.classList.replace('fa-eye', 'fa-eye-slash');
            e.target.previousElementSibling.setAttribute('type', 'text')
        }
        else {
            e.target.classList.replace('fa-eye-slash', 'fa-eye');
            e.target.previousElementSibling.setAttribute('type', 'password')
        }
    }
})

repeatPassword.addEventListener('focus', () => {
    document.querySelector('.error').innerText = '';
})

submitButton.addEventListener('click', checkPasswordEquality)